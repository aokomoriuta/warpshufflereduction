#include <stdio.h>
#include <iostream>

#include "kernel.hpp"

__global__
static void kernel(double* z, const double x[], const double y[])
{
	const auto i = (blockIdx.x * blockDim.x + threadIdx.x);

	const auto xy = x[i] * y[i];

	auto sum = xy;
	sum += __shfl_down(sum, 1);
	sum += __shfl_down(sum, 2);
	sum += __shfl_down(sum, 4);
	sum += __shfl_down(sum, 8);
	sum += __shfl_down(sum, 16);

	if(i == 0)
	{
		*z = sum;
	}
}


void Kernel(double* z, const double x[], const double y[],
        const std::size_t block, const std::size_t thread)
{
	kernel<<<block, thread>>>(z, x, y);
}

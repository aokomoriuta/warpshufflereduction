#ifndef KERNEL_HPP_INCLUDED
#define KERNEL_HPP_INCLUDED

void Kernel(double z[], const double x[], const double y[],
	const std::size_t block, const std::size_t thread);

#endif


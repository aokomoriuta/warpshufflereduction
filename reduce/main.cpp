#include <iostream>
#include <memory>
#include <cmath>
#include <cuda_runtime.h>

#include "kernel.hpp"

int main()
{
	cudaDeviceProp props;
	cudaGetDeviceProperties(&props, 0);
	std::cout << "Device" << props.name << std::endl;

	constexpr std::size_t N = 10;
	auto x = std::make_unique<double[]>(N);
	auto y = std::make_unique<double[]>(N);
	double z;

	for(auto i = decltype(N)(0); i < N; i++)
	{
		x[i] = std::sin(i + N);
		y[i] = std::cos(-i + N);
	}
	z = 0;

	double* xCuda;
	double* yCuda;
	double* zCuda;
	cudaMalloc(&xCuda, sizeof(double)*N);
	cudaMalloc(&yCuda, sizeof(double)*N);
	cudaMalloc(&zCuda, sizeof(double));

	cudaMemcpy(xCuda, x.get(), sizeof(double)*N, cudaMemcpyHostToDevice);
	cudaMemcpy(yCuda, y.get(), sizeof(double)*N, cudaMemcpyHostToDevice);

	Kernel(zCuda, xCuda, yCuda, 1, N);
	cudaDeviceSynchronize();

	cudaMemcpy(&z, zCuda, sizeof(double), cudaMemcpyDeviceToHost);

	{
		double expected = 0;
		for(auto i = decltype(N)(0); i < N; i++)
		{
			expected += x[i] * y[i];
		}
		const auto actual = z;
		std::cout << expected << ", " << actual << std::endl;
	}

	cudaFree(&xCuda);
	cudaFree(&yCuda);
	cudaFree(&zCuda);

	return 0;
}
